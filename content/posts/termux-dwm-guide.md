+++
title = "Termux dwm Guide"
author = ["Eric Nguyen"]
date = 2019-08-11
draft = false
+++

## Installation {#installation}


### Apps {#apps}

-   [Termux](https://termux.com/)
-   [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/android/)


### Termux {#termux}

Within Termux, execute the following commands:

```text
pkg install x11-repo dwm st tigervnc
echo "dwm &" > ~/.vnc/xstartup
echo "export DISPLAY=\":1\"" >> ~/.bashrc
```

**Notes:**

-   You can replace `dwm` with your preferred window manager such as `openbox` (I tried `i3`, but it didn't work for whatever reason).
-   We install `st` terminal here, but you can use whichever terminal you prefer that is available in Termux (e.g. `aterm`, `xterm`).
-   `~/.vnc/xstartup` is the file that specifies which programs are run on startup. In this case, we tell X to run `dwm` on startup.
-   Instead of using `echo`, you can use your preferred text editor such as `nano`, `vi`, or `ed`. If your text editor isn't available, you can simply install it by executing `pkg install` and your package name (e.g. `pkg install nano`).


## Run {#run}

To run dwm, you will need to start a local X server and then connect to it.
To do so, you can execute the following in Termux:

```text
vncserver -localhost
```

When you initialize a server with `vncserver` for the first time, you'll be prompted for password setup, it's fairly straightforward.
If you don't know what to answer for the last question just respond no.

```text
Would you like to enter a view-only password (y/n)? n
```


## Connect {#connect}

To connect to your newly hosted server, you can open the VNC Viewer app on your phone and add a new connection by pressing the circle "plus" button.
For your address, type `127.0.0.1:` and then your port number.
To calculate your port number, add your display number to 5900.
In this case, we use 1 as our display number, as set in our `~/.bashrc`, so we'd write in our address form, `127.0.0.1:5901`.
The name can be whatever you want it to be, but I simply use `Termux` for it.

Now that you've setup your connection, you can easily connect to it through the VNC Viewer menu and entering your password.
If you've successfully connected, congratulations!
If you have no idea how to use dwm, you can refer to [Dave's Visual Guide to dwm](https://ratfactor.com/slackware/dwm/).
Basically, `Alt+Shift+Enter` to open terminals, `Alt+Shift+c` to close windows.
If you were unable to connect to your server, refer to [Termux's official guide](https://wiki.termux.com/wiki/Graphical%5FEnvironment) on setting up a graphical environment, as I may have missed something.

**Note:** For some reason, I have trouble opening certain programs such as `mupdf`, `dmenu`, and `emacs`.
So if you've got the same problems, then I wouldn't worry too much about it, unless you've seen it work for other people.


## Clean up {#clean-up}

To kill a server (replace `1` with your server index):

```text
vncserver -kill :1
```
