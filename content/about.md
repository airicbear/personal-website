+++
title = "About"
author = ["Eric Nguyen"]
date = 2019-08-18
draft = false
+++

## Introduction {#introduction}

Hi!
I'm Eric Nguyen, but I go by the alias, "airicbear," online.
I'm an undergraduate first year at the main campus of Temple University majoring in [Data Science with a concentration in computation and modeling](https://bulletin.temple.edu/undergraduate/science-technology/computer-information-science/data-science-computation-modeling-bs/).

In my free time, I like to scroll through social news feeds (e.g. [Hacker News](https://news.ycombinator.com/)), explore new technologies (e.g. [GitHub Trending](https://github.com/trending)), and dance.


## Background {#background}


### Dance {#dance}

I started dancing back in elementary school around 4th grade when I was [introduced to dubstep](https://www.invidio.us/watch?v=fMDvZ6bdqZE) by my older brother.
I quickly picked up dance by watching other dancers such as RemoteKontrol on YouTube and would practice dancing all the time.
I thought I was the best dancer ever and all of my schoolmates would always ask me to show them my dance moves.
Everyone in school would know me as the kid who is good at dancing.

In 6th grade, I was introduced to b-boying, again by my older brother.
This style of dancing proved to be much more difficult for me to pick up, so I didn't dedicate much time to it like I did with dubstep.
I did, however, do b-boying for my SDL (Self-Directed Learning) project, so I did gain some basic skills from that.

I gradually lost interest in dancing since 7th grade until 9th grade, when I first joined my high school's [hip hop club](https://invidio.us/watch?v=ROZiVK4r3F4).
I would attend the club meetings every week and dance with or compete against other club members.
At first, I mostly incorporated popping style dancing to compensate for my lack of b-boying skills, but I eventually kept learning new moves; the backspin being my best.
Even though I was less experienced than others, my biggest advantage was my ability to dance on beat, which allowed me to win several club tournaments.

Once I got to 10th grade, most of the good dancers had already graduated or quit the club.
The beginning of the year was great, as new members had joined and so I had people to teach.
I was so excited to watch the new members grow into awesome b-boys/b-girls.
Little did I know back then that some of them would come to be some of my best friends.
Unfortunately, the club quickly started to die out and members would only occasionally show up.
One day, a member had told me of a newly created K-pop dance club that she joined and invited me to join as well.
I hadn't really listened to K-pop before other than Gangnam Style, but it wasn't like there was much to do in hip hop club anyway, and so I joined.
I honestly expected to only visit the club for a day or two and leave, since I didn't think K-pop would be for me.
To my surprise, at my first club meeting with them, I noticed that a fair number of the hip hop club members were already involved.
The first choreography we learned was for the song [_Rhythm Ta_ by iKON](https://invidio.us/watch?v=jdlDhEso650) and I was super clueless, but I tried my best.
I ended up making some of my best and worst memories in this club and the whole year was a roller coaster ride of emotions.
I did continue with the club in 11th grade, however it was much more laid back that year and less of a wild experience compared to 10th grade.

In 12th grade, I stopped practicing dancing and switched to a different school.
Perhaps I would have continued to dance with my friends back at my previous school if I had the social capacity to do so.
Unfortunately, I failed to stay in touch with them from social anxiety and I had no friends at my new school, so I focused my time and energy on learning math and coding instead.

Growing up as the quiet kid in school, dance really helped me socially and it was a great conversation starter.
I'm very thankful for my brother for introducing it to me in the first place and for the amazing people I had the chance to meet because of it.


### Programming {#programming}

As with everything, my older brother also introduced me to computer programming back in 7th grade.
He initially showed me how to create websites using HTML, CSS, and JavaScript.
I was quickly hooked and motivated to learn all about coding and making websites.
I pretty much learned all about coding through Codecademy, w3schools, and most importantly, my brother.
He would sit down with me all night to go over topics and help me with any issues and it was surprisingly fun, even though I often had no idea what he was talking about.
I remember how he would introduce me to a bunch of different things like Bootstrap, Angular, GitHub, Node.js, vim, etc. and it was all so overwhelming.
We also used to code together using LiveWeave and make random websites together.

After my first taste of coding with web development, I began to learn how to use the Unity game engine using JavaScript.
One of the games I made was a simple first-person game where the player would walk around with an axe and when it touches a tree, the tree disappears.
I hosted that game on my friend's Wix website.

In 8th grade, I joined a small coding club in school where I learned how to use the Logo programming language.
I also made a [ball rolling game](https://github.com/airicbear/8th-grade-science-fair) for my science fair project.

In 9th grade, I joined computer science club and had coding classes for my elective.
I wrote a bunch of websites this year [using Notepad++](https://airicbear.github.io/john-napier-project/images/scr01.png) and showed them to all of my friends through 000webhost.
One of the websites I did was for a history [project on John Napier](https://airicbear.github.io/john-napier-project/) and even though it wasn't amazing, I was super proud of it back then.

In 10th grade, I took an Alice & Java elective where I made cool animations using logic.
I mostly focused on dancing this year.

In 11th grade, I took AP Computer Science A and learned all about Java.
This is the year where I actually started understanding computer science for real.
I made a [bunch of projects](https://github.com/airicbear?utf8=✓&tab=repositories&q=apcs&type=&language=) in that class and was quite proud of them.
One of my biggest side projects of that year was my [Bootstrap portfolio website](https://hi-eric.000webhostapp.com/), where I learned how to use PHP to generate content from MySQL databases.
In the middle of the school year (Winter of 2018), I figured that there should be a way for me to put my code online, so I rediscovered Git and GitHub.
I regret not using it much earlier back when my brother showed me it; there are a number projects that I lost and would love to look back at.

In 12th grade, [I took Calculus](https://github.com/airicbear/calculus-homework) where I was the only student, so I tried out all sorts of cool things such as Python, R, Julia, Jupyter, LaTeX, KaTeX, Linux, pandoc, i3wm, etc.
Jupyter Notebooks were awesome since it supported math markup and plots, but it since my teacher printed out all of my completed assignments, I had to find an alternative.
This is when I found out about LaTeX.
First, I used Overleaf, which was pretty good; it even had vim keybindings.
But then I realized that I wanted to do my math homework on the bus, so I needed a way to compile my LaTeX offline.
Texmaker was super clunky to use and there were no vim keybindings, but it worked.
Finally, I found latexmk and I've been using it with vim as my primary way to write LaTeX.
In the last semester of the year, I started using this workflow to complete [physics assignments](https://github.com/airicbear/university-physics-modern) as well and it was awesome.
